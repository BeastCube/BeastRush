package net.beastcube.beastrush.king;

import net.minecraft.server.v1_8_R3.EntityVillager;
import net.minecraft.server.v1_8_R3.GenericAttributes;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;

import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 * @author BeastCube
 */
public class KingEntity extends EntityVillager {

    public KingEntity(Location loc) {
        super(((CraftWorld) loc.getWorld()).getHandle());
        this.locX = loc.getX();
        this.locY = loc.getY();
        this.locZ = loc.getZ();
        try {
            Field b = this.goalSelector.getClass().getDeclaredField("b");
            b.setAccessible(true);
            b.set(this.goalSelector, new ArrayList());
        } catch (IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
        }
        this.getAttributeInstance(GenericAttributes.maxHealth).setValue(200D);
        this.getAttributeInstance(GenericAttributes.MOVEMENT_SPEED).setValue(0D);
    }

    public Location getLocation() {
        return new Location(this.world.getWorld(), this.locX, this.locY, this.locZ);
    }

    @Override
    public void move(double d0, double d1, double d2) {

    }


}
