package net.beastcube.beastrush;

import lombok.Getter;
import net.beastcube.api.commons.minigames.MinigameType;
import net.beastcube.beastrush.king.KingEntity;
import net.beastcube.beastrush.kits.Kits;
import net.beastcube.minigameapi.Minigame;
import net.beastcube.minigameapi.MinigameAPI;
import net.beastcube.minigameapi.defaults.Defaults;
import net.beastcube.minigameapi.defaults.PlayerAmountBoundType;
import net.beastcube.minigameapi.defaults.PlayerAmountBounds;
import net.beastcube.minigameapi.entityregistry.EntityRegistrationEntry;
import net.beastcube.minigameapi.entityregistry.EntityRegistry;
import net.beastcube.minigameapi.events.GameStartEvent;
import net.beastcube.minigameapi.kits.KitChooserInteractItem;
import net.beastcube.minigameapi.kits.KitChoosing;
import net.beastcube.minigameapi.maps.MapChooserInteractItem;
import net.beastcube.minigameapi.maps.MapChoosing;
import net.beastcube.minigameapi.scoreboard.ScoreboardMode;
import net.beastcube.minigameapi.team.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author BeastCube
 */
@Minigame(arena = BeastRushArena.class, team = BeastRushTeam.class, minigameType = MinigameType.BEASTRUSH, scoreboardMode = ScoreboardMode.TEAMS_WITH_NO_PLAYERS)
public class BeastRush extends JavaPlugin implements Listener {

    @Getter
    private static MinigameAPI<BeastRushArena, BeastRushTeam, BeastRushConfig> minigameAPI;

    @Override
    public void onEnable() {
        super.onEnable();
        Defaults defaults = new Defaults();
        defaults.setBlockBreaking(false);
        defaults.setBlockPlacing(false);
        defaults.setCrafting(false);
        defaults.setAutoRespawn(true);
        defaults.setDropStuffOnDeath(false);
        defaults.setDaylightCycle(false);
        defaults.setEnterBedAllowed(false);

        minigameAPI = new MinigameAPI<>(this, new PlayerAmountBounds(PlayerAmountBoundType.MIN_MAX, 2, 32), defaults, BeastRushArena.class, BeastRushTeam.class, new BeastRushConfig());

        for (int i = 0; i < 2; i++) {
            minigameAPI.addTeam(new BeastRushTeam(TeamColor.getName(i), TeamColor.getColor(i)));
        }

        MapChoosing.setMapChooser(MapChooserInteractItem.getMapChooser());
        //MapChoosing.setMapValidator(new ColorSetValidator(colorset));
        TeamChoosing.setTeamChooser(TeamChooserInteractItem.getTeamChooser());
        KitChoosing.setKitChooser(KitChooserInteractItem.getKitChooser());

        Kits.register();

        Bukkit.getPluginManager().registerEvents(this, this);

    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

    //---------------------------------------------------------------------------------------------------\\
    //------------------------------------ Minigame Events ----------------------------------------------\\
    //---------------------------------------------------------------------------------------------------\\

    @EventHandler
    public void onGameStart(GameStartEvent e) {
        spawnKings();
        for (Player p : minigameAPI.getPlayersIngame()) {
            Team team = minigameAPI.getPlayersTeam(p);
            p.sendMessage(ChatColor.GOLD + "Das Spiel hat begonnen! Du bist im Team " + ColorConverter.colorToChatColor(team.getColor()) + team.getName());
        }
    }

    //---------------------------------------------------------------------------------------------------\\
    //----------------------------------------- Events --------------------------------------------------\\
    //---------------------------------------------------------------------------------------------------\\


    //---------------------------------------------------------------------------------------------------\\
    //----------------------------------------- Helpers -------------------------------------------------\\
    //---------------------------------------------------------------------------------------------------\\

    public void spawnKings() {
        for (Team t : minigameAPI.getTeams()) {
            BeastRushTeam team = (BeastRushTeam) t;
            BeastRushArena arena = minigameAPI.getArena();
            Location loc = arena.getKings().get(team.getName());
            team.setKing(spawnKing(loc));
        }
    }

    public static KingEntity spawnKing(final Location loc) {
        final KingEntity king = new KingEntity(loc);
        EntityRegistry.spawn(new EntityRegistrationEntry("King", 121, king), loc);
        return king;
    }

}
