package net.beastcube.beastrush.kits;

import net.beastcube.minigameapi.kits.ItemStackKit;
import net.beastcube.minigameapi.kits.KitChoosing;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * @author BeastCube
 */
public final class Kits {
    private static int id = -1;

    private static int getId() {
        id++;
        return id;
    }

    private static Inventory createInventory(ItemStack[] stacks) {
        Inventory inv = Bukkit.createInventory(null, InventoryType.PLAYER);
        for (int i = 0; i < stacks.length; i++)
            inv.setItem(i, stacks[i]);
        return inv;
    }

    public static final ItemStackKit DEFAULT = new ItemStackKit("Starter", "Starter Kit",
            Bukkit.createInventory(null, InventoryType.PLAYER), null, null, getId(), Material.EMERALD, (short) 0);

    public static void register() {
        KitChoosing.registerKit(DEFAULT);
    }
}
