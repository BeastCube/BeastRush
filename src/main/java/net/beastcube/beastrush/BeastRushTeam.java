package net.beastcube.beastrush;

import net.beastcube.beastrush.king.KingEntity;
import net.beastcube.minigameapi.team.Team;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;

/**
 * @author BeastCube
 */
public class BeastRushTeam extends Team {

    private KingEntity king;

    public BeastRushTeam(String name, Color color) {
        super(name, color);
    }

    @Override
    public String getScoreboardDisplayName() {
        return (hasKing() ? ChatColor.GREEN + "✔ " : ChatColor.RED + "✖ ") + getDisplayName();
    }

    public boolean hasKing() {
        return king != null;
    }

    public KingEntity getKing() {
        return king;
    }

    public void setKing(KingEntity king) {
        this.king = king;
    }

    public boolean isKing(Location loc) {
        return BeastRush.getMinigameAPI().getTeam(BeastRush.getMinigameAPI().getArena().isKing(loc)) == this;
    }

}
