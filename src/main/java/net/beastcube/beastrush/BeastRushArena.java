package net.beastcube.beastrush;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import net.beastcube.api.bukkit.commands.Arg;
import net.beastcube.api.bukkit.commands.Command;
import net.beastcube.api.bukkit.util.serialization.Serialize;
import net.beastcube.minigameapi.arena.Status;
import net.beastcube.minigameapi.arena.TeamArena;
import net.beastcube.minigameapi.util.SmoothSerializer;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author BeastCube
 */
@AllArgsConstructor
public class BeastRushArena extends TeamArena {

    @Serialize
    @Status(name = "Bronzespawner", command = "/addBronze")
    @Getter
    @Setter
    List<Location> bronze = new ArrayList<>();
    @Serialize
    @Status(name = "Bronze", command = "/setBronzeStack")
    @Getter
    @Setter
    ItemStack bronzeStack = new ItemStack(Material.BRICK);
    @Serialize
    @Status(name = "Bronze-Häufigkeit", command = "/setBronzeFrequency")
    @Getter
    @Setter
    int bronzeFreq = 2 * 20;
    @Serialize
    @Status(name = "Könige", command = "/setKing")
    @Getter
    Map<String, Location> kings = new HashMap<>();


    public BeastRushArena() {
        super();
    }

    public String isKing(Location loc) {
        for (Map.Entry<String, Location> e : kings.entrySet()) {
            if (e.getValue().getBlockX() == loc.getBlockX() && e.getValue().getBlockY() == loc.getBlockY() && e.getValue().getBlockZ() == loc.getBlockZ()) {
                return e.getKey();
            }
        }
        return null;
    }

    //---------------------------------- Bronze ----------------------------------\\

    public void addBronze(Location loc) {
        bronze.add(loc);
    }

    public void removeBronze(Location loc) {
        for (int i = 0; i < bronze.size(); i++) {
            Location b = bronze.get(i);
            if (b.getBlockX() == loc.getBlockX() && b.getBlockY() == loc.getBlockY() && b.getBlockZ() == loc.getBlockZ())
                bronze.remove(i);
        }
    }

    public void clearBronze() {
        bronze.clear();
    }

    @Command(identifiers = "addBronze", description = "Fügt einen neuen Bronzespawner hinzu")
    public void addBronze(Player sender) {
        addBronze(sender.getLocation());
        sender.sendMessage(ChatColor.GREEN + "Es wurde ein neuer Bronzespawner an deiner aktuellen Position hinzugefügt.");
    }

    @Command(identifiers = "removeBronze", description = "Löscht den naheliegendsten Bronzespawner")
    public void removeBronze(Player sender) {
        removeNearestSpawner(bronze, sender);
    }

    @Command(identifiers = "clearBronze", description = "Löscht alle Bronzespawner")
    public void clearBronze(Player sender) {
        clearBronze();
        sender.sendMessage(ChatColor.GREEN + "Du hast erfolgreich alle Bronzespawner entfernt.");
    }

    private void removeNearestSpawner(List<Location> spawners, Player sender) {
        if (spawners.size() == 0)
            sender.sendMessage(ChatColor.RED + "Es existiert noch kein Spawn.");
        Location nearest = spawners.get(0);
        double smallestDistance = nearest.distanceSquared(sender.getLocation());
        double currentDistance;
        for (int i = 1; i < spawners.size(); i++) {
            currentDistance = spawners.get(i).distanceSquared(sender.getLocation());
            if (currentDistance < smallestDistance) {
                smallestDistance = currentDistance;
                nearest = spawners.get(i);
            }
        }
        if (smallestDistance > 9) {
            sender.sendMessage(ChatColor.YELLOW + "Der naheliegendste Spawnpunkt ist mehr als 3 Blöcke entfernt.");
            sender.sendMessage(ChatColor.YELLOW + "Er befindet sich bei: " + SmoothSerializer.serializeVector(nearest.toVector(), 2));
        } else {
            spawners.remove(nearest);
            sender.sendMessage(ChatColor.GREEN + "Der naheliegendste Spawnpunkt wurde erfolgreich entfernt.");
        }
    }

    @Command(identifiers = "setBronzeStack", description = "Setzt das Item, welches für Bronze gespawnt werden wird")
    public void setBronzeStackCommand(Player sender) {
        if (sender.getItemInHand() != null) {
            setBronzeStack(sender.getItemInHand());
            sender.sendMessage(ChatColor.GREEN + "Das Item für Bronze wurde erfolgreich gesetzt.");
        } else
            sender.sendMessage(ChatColor.RED + "Du musst ein Item in der Hand halten");
    }

    @Command(identifiers = "setBronzeFrequency", description = "Setzt die Spawnrate von Bronze in Ticks.")
    public void setBronzeFreq(Player sender, @Arg(name = "Häufigkeit", verifiers = "min[1]") int freq) {
        setBronzeFreq(freq);
        sender.sendMessage(ChatColor.GREEN + "Du hast erfolgreich die Spawnrate von Bronze auf " + freq + " Ticks gesetzt.");
    }

    //------------------------------- Könige -----------------------------------\\

    @SuppressWarnings("deprecation")
    @Command(identifiers = "setKing", description = "Setzt den König für ein Team")
    public void setBed(Player sender, @Arg(name = "Team") String team) {
        Location l = sender.getLocation();
        kings.put(team, new Location(sender.getWorld(), l.getBlockX(), l.getBlockY(), l.getBlockZ(), l.getYaw(), l.getPitch()));
        sender.sendMessage(ChatColor.GREEN + "Der König für " + team + " wurde erfolgreich gesetzt");
    }

    @Command(identifiers = "removeKing", description = "Entfernt den König für ein Team")
    public void removeBed(Player sender, @Arg(name = "Team") String team) {
        if (kings.containsKey(team)) {
            kings.remove(team);
            sender.sendMessage(ChatColor.GREEN + "Der König für " + team + " wurde erfolgreich entfernt.");
        } else {
            sender.sendMessage(ChatColor.RED + String.format("Das Team %s hat keinen König", team));
        }
    }

}
